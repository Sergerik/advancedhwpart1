package Part1.FormValidator;

public class MyBaseException extends RuntimeException{

    MyBaseException(String errorMessage){
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
}
