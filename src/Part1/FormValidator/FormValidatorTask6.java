package Part1.FormValidator;
/*Фронт со своей стороны не сделал обработку входных данных анкеты! Петя
        очень зол и ему придется написать свои проверки, а также кидать исключения,
        если проверка провалилась. Помогите Пете написать класс FormValidator со
        статическими методами проверки. На вход всем методам подается String str.
        a. public void checkName(String str) — длина имени должна быть от 2 до 20
        символов, первая буква заглавная.
        b. public void checkBirthdate(String str) — дата рождения должна быть не
        раньше 01.01.1900 и не позже текущей даты.
        c. public void checkGender(String str) — пол должен корректно матчится в
        enum Gender, хранящий Male и Female значения.
        d. public void checkHeight(String str) — рост должен быть положительным
        числом и корректно конвертироваться в double.*/

import javax.naming.InvalidNameException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;


public class FormValidatorTask6 {
    enum Gender{
Female, Male
    }

    static void checkName(String str) throws InvalidNameException {
        if (str.length() >= 2 && str.length() <= 20 && Character.isUpperCase(str.charAt(0)))
            System.out.println("Имя корректно");
        else
            throw new InvalidNameException();
            }

    public static void checkBirthdate(String str) throws InvalidBirthdateException{
        Date birthdate = new Date(1);
        Date minBirthdate = new Date(1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
        birthdate = formatter.parse(str);
        minBirthdate = formatter.parse("01.01.1900");
        } catch (ParseException e) {
        e.printStackTrace();
        }
        Date maxBirthdate = new Date();

        if (birthdate.after(minBirthdate) && birthdate.before(maxBirthdate))
        System.out.println("Дата рождения корректна");
        else
        throw new InvalidBirthdateException();
    }

    public static void checkGender(String str) throws InvalidGenderException {
        Gender female = Gender.Female;
        Gender male = Gender.Male;
        if (female.toString().equals(str) || male.toString().equals(str))
            System.out.println("Пол корректен");
        else
            throw new InvalidGenderException();
    }

    public static void checkHeight(String str) throws InvalidHeightException {
        if (Double.parseDouble(str) > 0 &&
                (Pattern.matches("\\d\\.\\d\\d", str) || Pattern.matches("\\d\\.\\d", str)))
            System.out.println("Рост корректен");
        else
            throw new InvalidHeightException();
    }


}
