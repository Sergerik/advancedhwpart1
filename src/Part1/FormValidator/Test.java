package Part1.FormValidator;

public class Test {
    public static void main(String[] args) {
        FormValidatorTask6 formValidator = new FormValidatorTask6();
        // Вводим корректные данные
        try {
            FormValidatorTask6.checkName("Io");
            FormValidatorTask6.checkBirthdate("20.04.1989");
            FormValidatorTask6.checkGender("Male");
            FormValidatorTask6.checkHeight("1.74");
        } catch (InvalidNameException | InvalidBirthdateException | InvalidGenderException | InvalidHeightException |
                 javax.naming.InvalidNameException ex) {
            ex.getMessage();
        }

        try {
            FormValidatorTask6.checkName("ivan");
        } catch (InvalidNameException | javax.naming.InvalidNameException ex) {
            ex.getMessage();
        }

        try {
            FormValidatorTask6.checkBirthdate("20.03.1899");
        } catch (InvalidBirthdateException ex) {
            ex.getMessage();
        }

        try {
            FormValidatorTask6.checkGender("elameF");
        } catch (InvalidGenderException ex) {
            ex.getMessage();
        }

        try {
            FormValidatorTask6.checkHeight("0.739");
        } catch (InvalidHeightException ex) {
            ex.getMessage();
        }
    }
}
