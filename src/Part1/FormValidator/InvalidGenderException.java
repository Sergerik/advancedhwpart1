package Part1.FormValidator;

public class InvalidGenderException extends MyBaseException{

    InvalidGenderException(String errorMessage){
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidGenderException(){
        super("Некорректный пол");
    }
}
