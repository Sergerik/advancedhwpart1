package Part1.FormValidator;


public class InvalidBirthdateException extends MyBaseException{

    InvalidBirthdateException(String errorMessage){
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidBirthdateException(){
        super("Некорректная дата рождения");
    }
}
