package Part1.FormValidator;

public class InvalidHeightException extends MyBaseException{

    InvalidHeightException(String errorMessage){
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    InvalidHeightException(){
        super("Некорректный рост");
    }
}
