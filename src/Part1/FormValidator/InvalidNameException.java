package Part1.FormValidator;

public class InvalidNameException extends MyBaseException{

    InvalidNameException(String errorMessage){
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }
    InvalidNameException(){
        super("Некорректное имя");
    }
}
