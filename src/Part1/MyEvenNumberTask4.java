package Part1;

//Создать класс MyEvenNumber, который хранит четное число int n. Используя
//исключения, запретить создание инстанса MyEvenNumber с нечетным числом.

public class MyEvenNumberTask4 {
    public static void main(String[] args) {
        try {
            MyEvenNumber number_1 = new MyEvenNumber(6);
            MyEvenNumber number_2 = new MyEvenNumber(7);
        } catch (NotIsEvenNumberException ex) {
            ex.getMessage();
        }
    }
    public static class NotIsEvenNumberException extends MyBaseException {

        public NotIsEvenNumberException(String errorMessage) {
            super(errorMessage);
            System.out.println("LOG: " + errorMessage);
        }

        public NotIsEvenNumberException() {
            super("Переданное число не является четным");
        }
    }
    public static class MyBaseException extends RuntimeException {
        public MyBaseException(String errorMessage) {
            super(errorMessage);
            System.out.println("LOG: " + errorMessage);
        }
    }

    public static class MyEvenNumber {
        private int n = 2;
        MyEvenNumber(int n) throws NotIsEvenNumberException {
            if (n % 2 == 0) {
                this.n = n;
                System.out.println("Экземпляр класса со значением " + n + " успешно создан");
            } else
                throw new NotIsEvenNumberException();
        }
    }

}

