package Part1;

import java.util.Scanner;

public class AdvancedTask2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);


        int n = console.nextInt();

        int[] array = new int[n];


        for (int i = 0; i < n; i++)
            array[i] = console.nextInt();

        int p = console.nextInt();

        System.out.println(binarySearch(array, p));
    }

    public static int binarySearch(int[] array, int p) {
        int mid = array.length / 2;
        int high = array.length - 1;
        int low = 0;

        while (low <= high) {
            if (p < array[mid]) {
                high = mid - 1;
                mid = low + (high - low) / 2;
            } else if (p > array[mid]) {
                low = mid + 1;
                mid = low + (high - low) / 2;
            } else {
                return mid;
            }
        }
        return -1;
    }
}
