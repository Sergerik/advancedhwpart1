package Part1;

import java.util.Scanner;

public class MyUncheckedExceptionTask2 {
    public static void main(String[] args) throws MyUncheckedException {
        System.out.println("Введите число для деления на 10: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        try {
            System.out.println(10 / n);
        } catch (ArithmeticException e) {
            throw new MyUncheckedException("ОШИБКА ВО ВРЕМЯ ВЫПОЛНЕНИЯ. " +
                    "АРИФMЕТИЧЕСКАЯ ОШИБКА. ");
        }
    }

    public static class MyUncheckedException extends ArithmeticException {
        public MyUncheckedException(String message) {
            super(message);
        }
    }
}
