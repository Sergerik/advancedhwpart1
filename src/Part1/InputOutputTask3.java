package Part1;
//Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
//        ./output.txt текст из input, где каждый латинский строчный символ заменен на
//        соответствующий заглавный. Обязательно использование try с ресурсами.

import java.io.*;

public class InputOutputTask3 {

    public static void main(String[] args) throws IOException {

        final BufferedReader reader = new BufferedReader(new FileReader("D:\\input.txt"));
        final BufferedWriter writer = new BufferedWriter(new FileWriter("D:\\output.txt"));
        try (reader; writer) {
            String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line.toUpperCase());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
