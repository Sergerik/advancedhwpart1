package Part1;

import java.util.Scanner;

public class MyCheckedExceptionTask1 {
    public static void main(String[] args) throws MyCheckedException {
        System.out.println("Введите востраст: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n >= 18) {
            System.out.println("Вам уже исполнелось 18 ");
        } else {
            throw new MyCheckedException("ВАМ НЕТ 18 ЛЕТ. ВОЗРАСТ ДОЛЖЕН БЫТЬ 18 ИЛИ БОЛЕЕ. ");
        }
    }

    public static class MyCheckedException extends Exception {
        public MyCheckedException(String message) {
            super(message);
        }
    }
}