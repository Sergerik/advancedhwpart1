package Part1;

import java.util.Scanner;

//Найти и исправить ошибки в следующем коде (сдать исправленный вариант)
public class ChainCodeTask5 {
    public static void main(String[] args) throws Exception {
        inputN();
    }

    private static void inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n < 100 && n > 0) {
            System.out.println("Успешный ввод!");
        } else {
            throw new Exception("Неверный ввод");
        }
    }
}
