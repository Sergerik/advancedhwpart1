package Part1;

import java.util.Scanner;

public class AdvancedTask1 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        int n = console.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++)
            array[i] = console.nextInt();

        int highMax = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (int x : array) {
            if (x > highMax) {
                secondMax = highMax;
                highMax = x;
            } else if (x > secondMax)
                secondMax = x;
        }
        System.out.println(highMax + " " + secondMax);
    }
}
